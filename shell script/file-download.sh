#!/bin/bash

# Mengecek jaringan pada file check.sh
source check.sh
# Memanggil fungsi pada file create-folder.sh
source create-fldr.sh

# Memanggil fungsi pada file download-file.sh
source download.sh

# Memanggil fungsi pada file move-file.sh
source move.sh

# Menampilkan pesan ketika proses selesai
echo "Proses selesai." 
echo "File berhasil didownload dan dipindahkan ke folder $foldername dengan nama $filename dan tipe file yang sama."
