#!/bin/bash
# Memeriksa koneksi internet
function check_internet {
  wget -q --spider http://google.com

  if [ $? -eq 0 ]; then
    echo "Koneksi internet terdeteksi."
    return 0
  else
    echo "Koneksi internet tidak terdeteksi."
    return 1
  fi
}
# Meminta URL file dari pengguna
echo "Masukkan URL file yang ingin didownload: "
read url

# Nama file yang akan disimpan
echo "Masukkan nama file yang ingin disimpan: "
read filename

# Mendownload file dari website
if command -v wget &> /dev/null; then
  wget -O "$filename" "$url"
elif command -v curl &> /dev/null; then
  curl -o "$filename" "$url"
else
  echo "Program tidak dapat mendownload file karena tidak ada program wget atau curl yang tersedia."
  exit 1
fi
