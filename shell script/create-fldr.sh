#!/bin/bash

# Meminta nama folder dari pengguna
echo "Masukkan nama folder yang ingin dibuat: "
read foldername

# Membuat folder baru jika belum ada
if [ ! -d "$foldername" ]; then
  mkdir -p "$foldername"
  echo "Folder $foldername berhasil dibuat"
else
  echo "Folder $foldername sudah ada, tidak membuat folder baru"
fi
